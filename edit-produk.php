<?php
session_start();
include 'db.php';
if($_SESSION['status_login'] !=true){
  echo '<script>window.location="login.php"</script>';
}
$produk = mysqli_query($conn,"SELECT *FROM tb_product WHERE product_id ='".$_GET['id']."' ");
$p =mysqli_fetch_object($produk);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website E~Commerce</title>
    <link rel="stylesheet" href="admin.css">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
    <script src="hamburger.js"></script>
</head>
<body>
    <nav>
        <div class="wrapper">
            <div class="logo"><a href=''>Whysz Store</a></div>
            <div class="toggle">
              <i class="ham-btn fa-solid fa-bars"></i>
            </div>
            <div class="menu">
                <ul>
                    <li><a href= "admin.php"class=>Dashboard</a></li>
                    <li><a href= "profil.php"class=>Profil</a></li>
                    <li><a href= "data-kategori.php"class=>Data kategori</a></li>
                    <li><a href= "data-produk.php"class=>Data Product</a></li>
                    <li><a href= "logout.php"class="tbl-biru">Logout</a></li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="section">
      <div class="container">
        <h3>Edit Data Produk</h3>
        <div class= "box">
          <form action="" method="POST" enctype="multipart/form-data">
            
            <input type="text" name= "nama" class="input-control" placeholder="Nama Produk" value="<?php echo $p-> product_name?>"required>
            <input type="text" name= "harga" class="input-control" placeholder="Harga" value="<?php echo $p-> product_price?>"required>

            <img src="produk/<?php echo $p->product_image?>" width="100px">
            <input type="hidden" name="foto" value ="<?php echo $p->product_image?>">
            <input type="file" name= "gambar" class="input-control">
            <textarea class="input-control" name="deskripsi" placeholder="Deskripsi"><?php echo $p-> product_description?></textarea><br>
            <select class="input-control" name="status">
              <option value="">--Pilih--</option>
              <option value="1"<?php echo ($p->product_status==1)?'selected':'';?>>Aktif</option>
              <option value="o"<?php echo ($p->product_status==0)?'selected':'';?>>Tidak Aktif</option>
            </select>
            <input type="submit" name= "submit" value="Submit" class="btn">
          </form>
          <?php
              if(isset($_POST['submit'])){

                //data inputan dari form
               $kategori  = $_POST['kategori'];
               $nama      = $_POST['nama'];
               $harga     = $_POST['harga'];
               $deskripsi = $_POST['deskripsi'];
               $status    = $_POST['status'];
               $foto      = $_POST['foto'];

                //data gambar yang baru
               $filename=$_FILES['gambar']['name'];
               $tmp_name=$_FILES['gambar']['tmp_name'];

               


                // jika admin ganti gambar
                if($filename !=''){
              $type1= explode('.',  $filename);
              $type2= $type1[1];

               $newname='produk'.time().'.'.$type2;


               // menampung data format file yang diizinkan
               $tipe_diizinkan= array('jpg','jpeg','png','webp');

                  // validasi format file
                  if(!in_array($type2,$tipe_diizinkan)){
                  echo '<script>alert("Format file tidak diizinkan")</script>';
                }else{
                  unlink('./produk/'.$foto);
                  move_uploaded_file($tmp_name,'./produk/'.$newname);
                  $namagambar=$newname;
                }


                }else{
                  //jika admin tidak ganti gambar
                  $namagambar=$foto;

                }

                //query update produk
                $update = mysqli_query($conn,"UPDATE tb_product SET
                                    
                                      product_name = '".$nama."',
                                      product_price = '".$harga."',
                                      product_description = '".$deskripsi."',
                                      product_image = '".$namagambar."',
                                      product_status = '".$status."'
                                      WHERE product_id ='".$p->product_id."'  ");
                
                if($update){
                      echo '<script>alert("Tambah data berhasil")</script>';
                      echo '<script>window.location="data-produk.php"</script>';
                            }else{
                           echo 'gagal'.mysqli_error($conn);
                             }

              
               } 

          ?>
          
               
        </div>

       
</div>
<footer class="footer-Backgroud">

    <div class="footer-kiri">
      <p class="footer-link">
        <a href="index.html">Home</a>
        |
        <a href="produk.php">Product</a>
        |
        <a href="about.php">About</a>
        |
        <a href="contact.php">hubungi kami</a>
      </p>

      <p class="footer-name">© Wisnu Aryo Putro</p>
    </div>

    <div class="footer-center">
      <div>
        <p>+6287860348408</p>
      </div>
      <div>
        <p><a href="">wisnu_2005101106@mhs.unipma.ac.id</a></p>
      </div>
    </div>
    <div class="footer-kanan">
      <p class="footer-about">
        <span>Whisz Store Copyrigh©2022 </span>
    </div>
</footer>
<script>
   CKEDITOR.replace( 'deskripsi' );
 </script>
</div>
</html>