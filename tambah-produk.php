<?php
session_start();
include 'db.php';
if($_SESSION['status_login'] !=true){
  echo '<script>window.location="login.php"</script>';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website E~Commerce</title>
    <link rel="stylesheet" href="admin.css">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
    <script src="hamburger.js"></script>
</head>
<body>
    <nav>
        <div class="wrapper">
            <div class="logo"><a href=''>Whysz Store</a></div>
            <div class="toggle">
              <i class="ham-btn fa-solid fa-bars"></i>
            </div>
            <div class="menu">
                <ul>
                    <li><a href= "admin.php"class=>Dashboard</a></li>
                    <li><a href= "profil.php"class=>Profil</a></li>
                    <li><a href= "data-kategori.php"class=>Data kategori</a></li>
                    <li><a href= "data-produk.php"class=>Data Product</a></li>
                    <li><a href= "logout.php"class="tbl-biru">Logout</a></li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="section">
      <div class="container">
        <h3>Tambah Data Produk</h3>
        <div class= "box">
          <form action="" method="POST" enctype="multipart/form-data">
            <select class="input-control" name="kategori" required>
              <option value="">--Pilih--</option>
              <?php
                  $kategori = mysqli_query($conn, "SELECT *FROM tb_kategory ORDER BY kategory_id DESC");
                  while($r=mysqli_fetch_array($kategori)){
              ?>
              <option value="<?php echo $r['kategory_id'] ?>"><?php echo $r['kategory_name']?></option>
              <?php }?>
            </select>
            <input type="text" name= "nama" class="input-control" placeholder="Nama Produk" required>
            <input type="text" name= "harga" class="input-control" placeholder="Harga" required>
            <input type="file" name= "gambar" class="input-control" required>
            <textarea class="input-control" name="deskripsi" placeholder="Deskripsi"></textarea>
            <select class="input-control" name="status">
              <option value="">--Pilih--</option>
              <option value="1">Aktif</option>
              <option value="o">Tidak Aktif</option>
            </select>
            <input type="submit" name= "submit" value="Submit" class="btn">
          </form>
          <?php
              if(isset($_POST['submit'])){

               // print_r($_FILES['gambar']);
               // menampung inputan dari form
               $kategori  = $_POST['kategori'];
               $nama      = $_POST['nama'];
               $harga     = $_POST['harga'];
               $deskripsi = $_POST['deskripsi'];
               $status    = $_POST['status'];
               // menamgpung data file yang diupload
               $filename=$_FILES['gambar']['name'];
               $tmp_name=$_FILES['gambar']['tmp_name'];

               $type1= explode('.',  $filename);
               $type2= $type1[1];

               $newname='produk'.time().'.'.$type2;

               // menampung data format file yang diizinkan
               $tipe_diizinkan= array('jpg','jpeg','png','gif');

               // validasi format file
               if(!in_array($type2,$tipe_diizinkan)){
                echo '<script>alert("Format file tidak diizinkan")</script>';
               }else{

                move_uploaded_file($tmp_name,'./produk/'.$newname);

                $insert =mysqli_query($conn, "INSERT INTO tb_product VALUES(
                        null,
                        '".$kategori."',
                        '".$nama."',
                        '".$harga."',
                        '".$deskripsi."',
                        '".$newname."',
                        '".$status."',
                        null
                            )");
                  
                    if($insert){
                      echo '<script>alert("Tambah data berhasil")</script>';
                      echo '<script>window.location="data-produk.php"</script>';
                    }else{
                      echo 'gagal'.mysqli_error($conn);
                    }
                    }
                
               }

               // proses upload file sekaligus insert ke database

                
              

          ?>
          
               
        </div>

       
</div>
<footer class="footer-Backgroud">

    <div class="footer-kiri">
      <p class="footer-link">
        <a href="index.html">Home</a>
        |
        <a href="produk.php">Product</a>
        |
        <a href="about.php">About</a>
        |
        <a href="contact.php">hubungi kami</a>
      </p>

      <p class="footer-name">© Wisnu Aryo Putro</p>
    </div>

    <div class="footer-center">
      <div>
        <p>+6287860348408</p>
      </div>
      <div>
        <p><a href="">wisnu_2005101106@mhs.unipma.ac.id</a></p>
      </div>
    </div>
    <div class="footer-kanan">
      <p class="footer-about">
        <span>Whisz Store Copyrigh©2022 </span>
    </div>
</footer>
<script>
   CKEDITOR.replace( 'deskripsi' );
 </script>
</div>
</html>