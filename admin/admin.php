<?php
session_start();
if($_SESSION['status_login'] !=true){
  echo '<script>window.location="login.php"</script>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website E~Commerce</title>
    <link rel="stylesheet" href="admin.css">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="hamburger.js"></script>
</head>
<body>
    <nav>
        <div class="wrapper">
            <div class="logo"><a href=''>Whysz Store</a></div>
            <div class="toggle">
              <i class="ham-btn fa-solid fa-bars"></i>
            </div>
            <div class="menu">
                <ul>
                    <li><a href= "admin.php"class=>Dashboard</a></li>
                    <li><a href= "profil.php"class=>Profil</a></li>
                    <li><a href= "data-produk.php"class=>Data Product</a></li>
                    <li><a href= "logout.php"class="tbl-biru">Logout</a></li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="section">
      <div class="container">
        <h3>Dashboard</h3>
        <div class= "box">
          <h4>Selamat Datang <?php echo $_SESSION['a_global']->admin_name ?> di Toko Whysz Store</h4>
        </div>
</div>
<footer class="footer-Backgroud">

    <div class="footer-kiri">
      <p class="footer-link">
        <a href="index.html">Home</a>
        |
        <a href="produk.php">Product</a>
        |
        <a href="about.php">About</a>
        |
        <a href="contact.php">hubungi kami</a>
      </p>

      <p class="footer-name">© Wisnu Aryo Putro</p>
    </div>

    <div class="footer-center">
      <div>
        <p>+6287860348408</p>
      </div>
      <div>
        <p><a href="">wisnu_2005101106@mhs.unipma.ac.id</a></p>
      </div>
    </div>
    <div class="footer-kanan">
      <p class="footer-about">
        <span>Whisz Store Copyrigh©2022 </span>
    </div>
</footer>
</div>
</html>