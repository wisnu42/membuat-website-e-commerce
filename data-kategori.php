<?php
session_start();
include 'db.php';
if($_SESSION['status_login'] !=true){
  echo '<script>window.location="login.php"</script>';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website E~Commerce</title>
    <link rel="stylesheet" href="admin.css">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="hamburger.js"></script>
</head>
<body>
    <nav>
        <div class="wrapper">
            <div class="logo"><a href=''>Whysz Store</a></div>
            <div class="toggle">
              <i class="ham-btn fa-solid fa-bars"></i>
            </div>
            <div class="menu">
                <ul>
                    <li><a href= "admin.php"class=>Dashboard</a></li>
                    <li><a href= "profil.php"class=>Profil</a></li>
                    <li><a href= "data-kategori.php"class=>Data kategori</a></li>
                    <li><a href= "data-produk.php"class=>Data Product</a></li>
                    <li><a href= "logout.php"class="tbl-biru">Logout</a></li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="section">
      <div class="container">
        <h3>Data kategori</h3>
        <div class= "box">
          <p><a href="tambah-kategori.php">Tambah Data</a></p>
         <table border="1" cellspacing="0" class="table">
          <thead>
            <tr>
              <th width="60px">No</th>
              <th>Kategory</th>
              <th width="150px">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
                $no=1;
                $kategori=mysqli_query($conn,"SELECT *FROM tb_kategory ORDER BY kategory_id DESC");
                while($row= mysqli_fetch_array($kategori)){
            ?>
            <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $row['kategory_name']?></td>
              <td>
                <a href="edit-kategori.php?id=<?php echo $row['kategory_id']?>">Edit</a> || <a href="proses-hapus.php?idk=<?php echo $row['kategory_id']?>" onclick="return confirm('Yakin ingin hapus ?')">Hapus</a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
         </table>
        </div>
</div>
<footer class="footer-Backgroud">

    <div class="footer-kiri">
      <p class="footer-link">
        <a href="index.html">Home</a>
        |
        <a href="produk.php">Product</a>
        |
        <a href="about.php">About</a>
        |
        <a href="contact.php">hubungi kami</a>
      </p>

      <p class="footer-name">© Wisnu Aryo Putro</p>
    </div>

    <div class="footer-center">
      <div>
        <p>+6287860348408</p>
      </div>
      <div>
        <p><a href="">wisnu_2005101106@mhs.unipma.ac.id</a></p>
      </div>
    </div>
    <div class="footer-kanan">
      <p class="footer-about">
        <span>Whisz Store Copyrigh©2022 </span>
    </div>
</footer>
</div>
</html>