<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website E~Commerce</title>
    <link rel="stylesheet" href="produk.css">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="hamburger.js"></script>
</head>
<body>
    <nav>
        <div class="wrapper">
            <div class="logo"><a href=''>Whysz Store</a></div>
            <div class="toggle">
                <i class="ham-btn fa-solid fa-bars"></i>
              </div>
            <div class="menu">
                <ul>
					<li><a href= "index.php"class=>Home</a></li>
                    <li><a href= "produk.php"class=>Product</a></li>
                    <li><a href= "about.php"class=>Abouts</a></li>
                    <li><a href= "contact.php"class=>Contact</a></li>
                    <li><a href= "login.php"class="tbl-biru">Login</a></li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper">
        <!-- untuk produk -->
        <div id="produk">
            <div class="gambar">
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-ransel-lipat-pria-tribute-foldable-pack-15.jpg">
                    <h1>EIGER TRIBUTE FOLDABLE PACK 15</h1>
                    <p>Harga Rp290.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-ransel-laptop-pria-kanawa-commute-20-10.jpg">
                    <h1>EIGER KANAWA COMMUTE 20 1.0 LAPTOP BACKPACK</h1>
                    <p>Harga Rp489.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-ransel-laptop-pria-forlough-30.jpg">
                    <h1>EIGER FORLOUGH 30 LAPTOP BACKPACK</h1>
                    <p>Harga Rp859.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-selempang-pria-forlough-shoulder.jpg">
                    <h1>EIGER FORLOUGH SHOULDER BAG</h1>
                    <p>Harga Rp239.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-selempang-pria-wanderway-shoulder.jpg">
                    <h1>EIGER WANDERWAY SHOULDER BAG</h1>
                    <p>Harga Rp249.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/0/0/000000000910008281.jpg">
                    <h1>EIGER Z-KURUVINDA 2 2.0 TRAVEL POUCH</h1>
                    <p>Harga Rp249.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-ransel-pria-macaca-18.jpg">
                    <h1>EIGER MACACA 18 BACKPACK</h1>
                    <p>Harga Rp279.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-ransel-laptop-pria-selempang-kanawa-commute-30-10.jpg">
                    <h1>EIGER KANAWA COMMUTE 30 1.0 LAPTOP BACKPACK</h1>
                    <p>Harga Rp1.099.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                <div class="foto">
                    <img src="https://thumbor.sirclocdn.com/unsafe/293x293/filters:format(webp)/magento.eigeradventure.com/media/catalog/product/cache/c6e678ff4843a2306873597ce4b1ecc2/t/a/tas-ransel-laptop-pria-selempang-kanawa-commute-25-10.jpg">
                    <h1>EIGER KANAWA COMMUTE 25 1.0 LAPTOP BACKPACK</h1>
                    <p>Harga Rp749.000,00</p>
                    <a href="order.php">Beli Sekarang</a>
                </div>
                    <div class="copyright">
                        <!-- <h4 style="margin-top: 10px;"><br>Whisz Store Copyright&copy 2022 </br></h4> -->
                    </div>
                </div>
                </div>
            </div>
        </div>
<footer class="footer-Backgroud">
    <div class="footer-kiri">
        <p class="footer-link">
            <a href="index.php">Home</a>
            |
            <a href="produk.php">Product</a>
            |
            <a href="about.php">About</a>
            |
            <a href="contact.php">hubungi kami</a>
        </p>

        <p class="footer-name">© Wisnu Aryo Putro</p>
    </div>

    <div class="footer-center">
        <div>
            <p>+6287860348408</p>
        </div>
        <div>
            <p><a href="">wisnu_2005101106@mhs.unipma.ac.id</a></p>
        </div>
    </div>
    <div class="footer-kanan">
        <p class="footer-about">
            <span>Whisz Store Copyrigh©2022 </span>
    </div>
</footer>
</body>
</html>
                    