<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website E~Commerce</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="hamburger.js"></script>
</head>
<body>
    <nav>
        <div class="wrapper">
            <div class="logo"><a href=''>Whysz Store</a></div>
            <div class="toggle">
              <i class="ham-btn fa-solid fa-bars"></i>
            </div>
            <div class="menu">
                <ul>
                    <li><a href= "index.php"class=>Home</a></li>
                    <li><a href= "produk.php"class=>Product</a></li>
                    <li><a href= "about.php"class=>Abouts</a></li>
                    <li><a href= "contact.php"class=>Contact</a></li>
                    <li><a href= "login.php"class="tbl-biru">Login</a></li>
                    
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper">
        
            </div>
        </section>

        <section id="home">
            <div class="home"></div>
            <img src="https://img.freepik.com/free-vector/flat-adventure-background-with-illustration_23-2149044036.jpg?w=996&t=st=1667906723~exp=1667907323~hmac=19dc2c96aecfafcc81de1ea9fef67d664062c7a2c9f4c85d30055fe2f27e693f"alt="image" height="450px" width="630px">
            <div class="kolom">
                <p class ="deskripsi">Tas Keren & Kekinian</p>
                <h2>Whysz Store</h2>
                <p class ="tentang">Pick your style for your days  </p>
                <p>Dapatkan koleksi tas terbaru dan terkeren musim ini dari brand andalan kami hanya untuk kamu.Berlangganan ke milik Whysz Store untuk menerima pembaruan tentang pendatang baru, penawaran, dan informasi diskon lainnya</p>
                <p><a href="produk.php"class="tbl-biru">Mulai Berbelanja</a></p>
            </div>
        </section>
    </div>
</div>
<footer class="footer-Backgroud">

    <div class="footer-kiri">
      <p class="footer-link">
        <a href="index.html">Home</a>
        |
        <a href="produk.php">Product</a>
        |
        <a href="about.php">About</a>
        |
        <a href="contact.php">hubungi kami</a>
      </p>

      <p class="footer-name">© Wisnu Aryo Putro</p>
    </div>

    <div class="footer-center">
      <div>
        <p>+6287860348408</p>
      </div>
      <div>
        <p><a href="">wisnu_2005101106@mhs.unipma.ac.id</a></p>
      </div>
    </div>
    <div class="footer-kanan">
      <p class="footer-about">
        <span>Whisz Store Copyrigh©2022 </span>
    </div>
</footer>
</div>
</html>