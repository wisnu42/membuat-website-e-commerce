<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Website E~Commerce</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="hamburger.js"></script>
</head>
<body>
    <nav>
        <div class="wrapper">
            <div class="logo"><a href=''>Whysz Store</a></div>
            <div class="toggle">
              <i class="ham-btn fa-solid fa-bars"></i>
            </div>
            <div class="menu">
                <ul>
                    <li><a href= "index.php"class=>Home</a></li>
                    <li><a href= "produk.php"class=>Product</a></li>
                    <li><a href= "about.php"class=>Abouts</a></li>
                    <li><a href= "contact.php"class=>Contact</a></li>
                    <li><a href= "login.php"class="tbl-biru">Login</a></li>
                   
                </ul>
            </div>
        </div>
    </nav>
        <div class="wrapper">
             <!-- untuk abouts -->
        <section id="abouts">
            <div class="kolom">
                <p class="deskripsi">Produk kualitas terbaik plus diberikan bonus merchandise unik lagi? Hanya di Whysz Store tempatnya!</p>
                <h2>Online Shopping</h2>
                
                <p>Halaman produk Whysz Store tampak sangat elegan dengan desainnya tidak hanya tampak simpel dan modern, tapi navigasinya juga sangat mudah.

                    Foto-foto yang menunjukkan beragam lifestyle di halamannya membantu customer melihat bagaimana mereka bisa menggunakan produk ini dalam kesehariannya. Beberapa figur publik juga tampil sebagai model agar customer lebih percaya terhadap produk mereka.
                    Website ini juga memiliki tampilan yang menarik dengan palet warna cerah. Home page mereka menyampaikan penawaran produknya dengan teks yang efektif dan persuasif.
                    
                    Selain itu, halaman produk Whysz Store menggunakan tombol keranjang yang selalu tersedia di bagian bawah sehingga customer bisa langsung melakukan pembelian</p>
                <p><a href="index.php" class="tbl-hijau">Kembali ke Home</a></p>
    </div>
    <img src="https://img.freepik.com/free-psd/3d-illustration-cartoon-character-young-backpacker-man-holding-shopping-bag-smile-rendering_1150-65971.jpg?w=740&t=st=1667907684~exp=1667908284~hmac=9eeda5e50d528cb59b50fd93a1a0640d4b4d8a65c5647b743e1d4b5e3ef28b15"alt="image" height="480px" width="500px">
    </section>
    </div>
</div>
<footer class="footer-Backgroud">

    <div class="footer-kiri">
      <p class="footer-link">
        <a href="index.php">Home</a>
        |
        <a href="produk.php">Product</a>
        |
        <a href="about.php">About</a>
        |
        <a href="contact.php">hubungi kami</a>
      </p>

      <p class="footer-name">© Wisnu Aryo Putro</p>
    </div>

    <div class="footer-center">
      <div>
        <p>+6287860348408</p>
      </div>
      <div>
        <p><a href="">wisnu_2005101106@mhs.unipma.ac.id</a></p>
      </div>
    </div>
    <div class="footer-kanan">
      <p class="footer-about">
        <span>Whisz Store Copyrigh©2022 </span>
    </div>
</footer>
</div>
</html>