<HTML>

    <head>
      <title>Order page</title>
      <link rel="stylesheet" href="order.css">
      <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
      <script text="text/javascript" src="order.js"></script>
    </head>
    
    <body>
    
      <form class="whatsapp-form">
        <div class="datainput">
          <input class="validate" id="wa_name" name="name" required="" type="text" value='' />
          <label>Nama Pembeli</label>
        </div>

        <div class="datainput">
          <input class="validate" id="order_amount" name="amount" required="" type="text" value='' />
          <label>Nama Product</label>         
        </div>

        <div class="datainput">
          <select id="wa_select">
            <option hidden='hidden' selected='selected' value='default'>Methode Pembayaran</option>
            <option value="1">DANA</option>
            <option value="2">OVO</option>
            <option value="3">Transfer Bank</option>
          </select>
        </div>

        <div class="datainput">
          <input class="validate" id="wa_email" name="email" required="" type="email" value='' />
          <label>Your Email Address</label>
        </div>


        <div class="datainput">
          <input class="validate" id="wa_number" name="count" required="" type="text" value='' />
          <label>Your Phone Number</label>
        </div>

        <div class="datainput">
          <textarea id='wa_textarea' placeholder='' maxlength='5000' row='1' required=""></textarea>
          <label>Description</label>
        </div>

        <a class="send_form" href="javascript:void" type="submit" title="Send to Whatsapp">Send to Whatsapp</a>
        <div id="text-info"></div>
      </form>

    </body>
    
    </HTML>